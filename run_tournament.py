import sys
from itertools import product
from urllib2 import urlopen
from bots import battleline_bots
from random import shuffle

number_of_times = int(sys.argv[1]) if len(sys.argv) == 2 else 1

for _ in xrange(number_of_times):    
    all_match_ups = list(product(battleline_bots, battleline_bots))
    shuffle(all_match_ups)
    for bot1, bot2 in all_match_ups:
        if bot1 != bot2:
            print urlopen("http://127.0.0.1:8001/runServer/{}/{}".format(bot1, bot2)).read()

